import { createContext } from "react";


interface ToDoItemProps {
    id: string,
    name: string,
    isChecked: boolean,
}

interface PriorityItemProps {
    id: string,
    name: string,
    isChecked: boolean,
    taskIdToPriority?: string,
}

interface SharedState {
    items: ToDoItemProps[];
    setItems: React.Dispatch<React.SetStateAction<ToDoItemProps[]>>;
    priorities: PriorityItemProps[];
    setPriorities: React.Dispatch<React.SetStateAction<PriorityItemProps[]>>;
}

const ItemContext = createContext<SharedState | undefined>(undefined);

export default ItemContext;