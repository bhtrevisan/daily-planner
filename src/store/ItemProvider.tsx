import ItemContext from "./item-context";
import { useState, useContext, ReactNode } from "react";


type ToDoItemProps = {
    id: string,
    name: string,
    isChecked: boolean,
}

interface PriorityItemProps {
    id: string,
    name: string,
    isChecked: boolean,
    taskIdToPriority?: string,
}

interface SharedState {
    items: ToDoItemProps[];
    setItems: React.Dispatch<React.SetStateAction<ToDoItemProps[]>>;
    priorities: PriorityItemProps[];
    setPriorities: React.Dispatch<React.SetStateAction<PriorityItemProps[]>>;
}

interface SharedStateProviderProps {
    children: ReactNode;
}

const ItemProvider: React.FC<SharedStateProviderProps> = ({ children }: any) => {
    const defaultItemList: ToDoItemProps[] = [
        {
            id: "t1",
            name: "Task 1",
            isChecked: false,
        },
        {
            id: "t2",
            name: "Task 2",
            isChecked: false,
        },
        {
            id: "t3",
            name: "Task 3",
            isChecked: false,
        }
    ];

    const defaultIPriorityList: PriorityItemProps[] = [
        {
            id: "p1",
            name: "Priority 1",
            isChecked: true,
        },
        {
            id: "p2",
            name: "Priority 2",
            isChecked: false,
        },
        {
            id: "p3",
            name: "Priority 3",
            isChecked: false,
        }
    ];

    const [items, setItems] = useState<ToDoItemProps[]>(defaultItemList);
    const [priorities, setPriorities] = useState<PriorityItemProps[]>(defaultIPriorityList);
    
    return (
        <ItemContext.Provider value={{ items, setItems, priorities, setPriorities }}>
            {children}
        </ItemContext.Provider>
    );
}

export default ItemProvider;

export const useSharedState = (): SharedState => {
    const context = useContext(ItemContext);
    if(!context) {
        throw new Error("useSharedState must be used within an ItemProvider");
    }
    return context;
};