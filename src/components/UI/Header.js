import "./Header.css";

const Header = () => {
  return (
    <div className='headerDiv'>
      <h1>Daily Planner</h1>
    </div>
  );
};

export default Header;