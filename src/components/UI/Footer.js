import React from "react";
import classes from "./Footer.module.css";

const Footer = () => {
  const year = new Date().getFullYear();

  return (
    <div className={classes.footer}>
      <label>
        {`© ${year}`}<a href="https://linkedin.com/in/bhtrevisan" target="_blank"> Bruno Trevisan</a>
      </label>
    </div>
  );
};

export default Footer;