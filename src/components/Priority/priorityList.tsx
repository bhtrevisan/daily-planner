import React, { useState } from "react";
import { SortableContext, verticalListSortingStrategy } from '@dnd-kit/sortable';
import { useSharedState } from "../../store/ItemProvider";
import PriorityItem from "./priorityItem";
import { useDroppable } from "@dnd-kit/core";


const PriorityList: React.FC = () => {
    const { priorities, setPriorities, items, setItems } = useSharedState();
    const [showCompleted, setShowCompleted] = useState(false);
    const { setNodeRef } = useDroppable({ id: 'PriorityListDropplable', /*disabled: (priorities.length > 0) ? true : false*/ });

    const handleDoneAndUndoPriority = (id: string) => {
        const newPriorities = [...priorities];
        const index = newPriorities.findIndex(item => item.id === id);
        newPriorities[index].isChecked = !newPriorities[index].isChecked;
        setPriorities(newPriorities);
        if (newPriorities[index].taskIdToPriority && items[items.findIndex(item => item.id === newPriorities[index].taskIdToPriority)]) {
            console.log('newPriorities[index].taskIdToPriority: ', newPriorities[index].taskIdToPriority);
            const newItems = [...items];
            const itemIndex = newItems.findIndex(item => item.id === newPriorities[index].taskIdToPriority);
            newItems[itemIndex].isChecked = !newItems[itemIndex].isChecked;
            setItems(newItems);
        }
    }

    // console.log('priorities: ', priorities);

    return (
        <div className="priorityList" ref={setNodeRef}>
            <SortableContext items={priorities} strategy={verticalListSortingStrategy}>
                <h1>Top 3 Day Priorities</h1>
                {priorities && priorities.every(item => item.isChecked) && <h3>Set your top Priorities of the day</h3>}
                <div className="priorityContext" >
                    {priorities && priorities.map((item, index) => {
                        if (!item.isChecked) {
                            return <PriorityItem key={item.id} id={item.id} name={item.name} isChecked={item.isChecked} index={index} onDoneUndo={handleDoneAndUndoPriority} />
                        }
                    })}
                </div>
            </SortableContext>
            <button onClick={() => setShowCompleted(!showCompleted)} className='hideAndShow' >Hide/Show</button>
            {showCompleted && priorities && priorities.map((item, index) => {
                if (item.isChecked) {
                    return <PriorityItem key={item.id} id={item.id} name={item.name} isChecked={item.isChecked} index={index} onDoneUndo={handleDoneAndUndoPriority} />;
                }
                return null;
            })}
            {showCompleted && priorities.every(item => !item.isChecked) && <p>No tasks done</p>}
        </div>
    );
};

export default PriorityList;