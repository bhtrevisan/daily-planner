import { useSortable } from '@dnd-kit/sortable';
import { CSS } from "@dnd-kit/utilities";
import './priorities.css'

interface PriorityItemProps {
    id: string,
    name: string,
    isChecked?: boolean,
    index?: number,
    taskIdToPriority?: string,
    onDoneUndo?: any
}

const PriorityItem: React.FC<PriorityItemProps> = (props: PriorityItemProps) => {
    const { attributes, listeners, setNodeRef, transform, transition } = useSortable({ id: props.id, disabled: props.isChecked });

    const handlePriorityClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        props.onDoneUndo(props.id);
    }
    
    return (
        <div ref={setNodeRef} style={{ transform: CSS.Translate.toString(transform), transition: transition }} className={!props.isChecked ? "priorityItem" : "priorityItemChecked"}>
            <label className='priorityLabel' {...attributes} {...listeners}>{props.name}</label>
            <button onClick={handlePriorityClick}>{props.isChecked ? "Undo" : "Done"}</button>
        </div>
    );
}

export default PriorityItem;