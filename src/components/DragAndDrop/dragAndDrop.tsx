import { useState } from 'react';
import { useSharedState } from '../../store/ItemProvider';
import PriorityList from '../Priority/priorityList';
import ToDoList from '../ToDo/toDoList';
import { DndContext, closestCenter, useSensor, useSensors, PointerSensor, KeyboardSensor, TouchSensor, DragEndEvent, DragOverlay, DragStartEvent } from "@dnd-kit/core";
import { arrayMove, sortableKeyboardCoordinates } from '@dnd-kit/sortable';
import PriorityItem from '../Priority/priorityItem';
import ToDoItem from '../ToDo/toDoItem';
import '../../App.css';

const DragAndDrop: React.FC = () => {
    const [activeId, setActiveId] = useState<string>();
    const { items, setItems, priorities, setPriorities } = useSharedState();
    const [prioritykeyId, setPrioritykeyId] = useState(priorities.length + 1);
    const sensors = useSensors(
        useSensor(PointerSensor),
        useSensor(KeyboardSensor, {
            coordinateGetter: sortableKeyboardCoordinates,
        }),
        useSensor(TouchSensor, {
            // Press delay of 250ms, with tolerance of 5px of movement
            activationConstraint: {
                delay: 250,
                tolerance: 5,
            },
        })
    );


    const handleDragEnd = (event: DragEndEvent) => {
        if (!event.over) return;
        // console.log("event: ", event);

        // Move TASK to TASK - Reorder tasks
        if ((event.active.id !== event.over.id) && (event.active.id.toString().includes('t')) && (event.over.id.toString().includes('t'))) {
            console.log('Task to Task');
            setItems((items) => {
                const oldIndex = items.findIndex(item => item.id === event.active.id);
                const newIndex = items.findIndex(item => item.id === event.over?.id);

                return arrayMove(items, oldIndex, newIndex)
            });
        }

        // Move TASK to PRIORITY - Add priority
        if ((event.active.id !== event.over.id) && (event.active.id.toString().includes('t')) && (event.over.id.toString().includes('p'))) {
            console.log('Task to Priority');
            const newPriorityName = items[items.findIndex(item => item.id === activeId)].name;
            const taskToPriorityId = items[items.findIndex(item => item.id === activeId)].id;

            if (priorities.length === 0) {
                setPriorities(prevPriorities => [...prevPriorities, { id: `p${prioritykeyId}`, name: newPriorityName, isChecked: false, taskIdToPriority: taskToPriorityId }]);
            }
            else if (!priorities.some(item => item.taskIdToPriority === taskToPriorityId)) {
                setPriorities(prevPriorities => [...prevPriorities, { id: `p${prioritykeyId}`, name: newPriorityName, isChecked: false, taskIdToPriority: taskToPriorityId }]);
            }
            setPrioritykeyId(prevPrioritykeyId => prevPrioritykeyId + 1);
        }

        // Move PRIORITY to PRIORITY - Reorder priorities
        if (event.active.id !== event.over.id && (event.active.id.toString().includes('p')) && (event.over.id.toString().includes('p'))) {
            console.log('Priority to Priority');
            setPriorities((priorities) => {
                const oldIndex = priorities.findIndex(item => item.id === event.active.id);
                const newIndex = priorities.findIndex(item => item.id === event.over?.id);

                return arrayMove(priorities, oldIndex, newIndex)
            });
        }

        // Move PRIORITY to TASK - Remove priority
        if ((event.active.id !== event.over.id) && (event.active.id.toString().includes('p')) && ((event.over.id.toString().includes('t')) || (event.over.id === 'ToDoListDropplable'))) {
            console.log('Priority to Task');
            setPriorities(prevPriorities => prevPriorities.filter(item => item.id !== event.active.id));

            // Check if the priority.taskIdToPriority is in some id in the items array
            const priorityIndex = priorities.findIndex(item => item.id === event.active.id);
            const actualTaskIdToPriority = priorities[priorityIndex].taskIdToPriority;
            
            if (actualTaskIdToPriority && (!items.some(item => item.id === actualTaskIdToPriority) || items.length === 0)) {
                const newTaskId = actualTaskIdToPriority;
                setItems(prevItems => [...prevItems, { id: newTaskId, name: priorities[priorityIndex].name, isChecked: false }]);
            }
        }



        setActiveId('');
        // console.log('priorities: ', priorities);
        // console.log('items: ', items);
    }

    const handleDragOver = (event: DragEndEvent) => {
        // console.log("drag event: ", event);
        if (!event.over) return;

    }

    const handleDragStart = (event: DragStartEvent) => {
        setActiveId(event.active.id.toString());
    }


    return (
        <div className="appDiv">
            <DndContext sensors={sensors} collisionDetection={closestCenter} onDragStart={handleDragStart} onDragEnd={handleDragEnd} onDragOver={handleDragOver} >
                <ToDoList />
                <PriorityList />
                <DragOverlay>
                    {activeId ?
                        (
                            activeId.includes('t')
                                ? <ToDoItem id={activeId} name={items[items.findIndex(item => item.id === activeId)].name} isChecked={items[items.findIndex(item => item.id === activeId)].isChecked} />
                                : <PriorityItem id={activeId} name={priorities[priorities.findIndex(item => item.id === activeId)].name} />
                        )
                        : null}
                </DragOverlay>
            </DndContext>
        </div>
    );
}

export default DragAndDrop;