import { useEffect, useState } from 'react';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from "@dnd-kit/utilities";
import trashIcon from '../../icons/trashIcon.png';
import dragIcon from '../../icons/dragIcon.png';
import './toDo.css';


type ToDoItemProps = {
    id: string,
    name: string,
    isChecked: boolean,
    index?: number,
    onNameChange?: any,
    onCheck?: any,
    onRemove?: any
};

const ToDoItem: React.FC<ToDoItemProps> = (props: ToDoItemProps) => {
    const [itemName, setItemName] = useState<string>(props.name);
    const [isChecked, setIsChecked] = useState<boolean>(props.isChecked);
    const { attributes, listeners, setNodeRef, transform, transition } = useSortable({ id: props.id });

    useEffect(() => {
        setIsChecked(props.isChecked)
    }, [props])
    
    const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        // console.log("LOG HANDLER: ", event.target.value);
        setItemName(event.target.value);
        props.onNameChange(event.target.value, props.id);
    }

    const handleCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        // console.log("LOG HANDLER: ", event.target.checked);
        setIsChecked(event.target.checked);
        props.onCheck(event.target.checked, props.id);
    }

    const handleRemoveChange = (event: React.MouseEvent<HTMLButtonElement>) => {
        props.onRemove(props.id);
    }

    return (
        <div ref={setNodeRef} style={{ transform: CSS.Translate.toString(transform), transition: transition }}  className='toDoItemStyle'>
            <div className="toDoItemCheckboxStyle" >
                <input type="checkbox" checked={isChecked} name="completed" onChange={handleCheckboxChange} className='toDoItemCheckboxStyle' id={props.id} />
                <label htmlFor={props.id}>
                    <span></span>
                </label>
                <input type="text" name="todoInput" data-testid="todoInput" value={itemName} onChange={handleNameChange} className={isChecked ? 'toDoDoneItem' : 'toDoItemInput'} />
            </div>
            <button className='trashIconButton' onClick={handleRemoveChange} ><img src={trashIcon} alt='icon' className='trashIcon'></img></button>
            <button {...attributes} {...listeners} className='dragIconButton' ><img src={dragIcon} alt='icon' className='dragIcon'></img></button>
        </div>
    );
};

export default ToDoItem;