import { useEffect, useState } from "react";
import { SortableContext, useSortable, verticalListSortingStrategy } from '@dnd-kit/sortable';
import ToDoItem from "./toDoItem";
import { useSharedState } from "../../store/ItemProvider";
import './toDo.css';
import { useDroppable } from "@dnd-kit/core";

type ToDoListProps = {
};


const ToDoList: React.FC<ToDoListProps> = () => {
    const { items, setItems, priorities, setPriorities } = useSharedState();
    const [keyId, setKeyId] = useState(items.length + 1);
    const { setNodeRef } = useDroppable({ id: 'ToDoListDropplable' });


    const addNewItem = () => {
        setKeyId(prevKeyId => prevKeyId + 1);
        setItems(prevItems => [...prevItems, { id: `t${keyId}`, name: "", isChecked: false }]);
    }

    const removeItem = (id: string) => {
        setItems(prevItems => prevItems.filter(item => item.id !== id));
    }

    const handleNameChange = (name: string, id: string) => {
        // console.log("ID: ", id);
        // console.log("name: ", name);
        const newItems = [...items];
        const index = newItems.findIndex(item => item.id === id);
        newItems[index].name = name;
        setItems(newItems);
        if (priorities.some(item => item.taskIdToPriority === newItems[index].id)) {
            const newPriorities = [...priorities];
            const pIndex = newPriorities.findIndex(item => item.taskIdToPriority === newItems[index].id);
            newPriorities[pIndex].name = name;
            setPriorities(newPriorities);
        }
    }

    const handleCheckboxChange = (itemIsChecked: boolean, id: string) => {
        // console.log("ID: ", id);
        // console.log("itemIsChecked: ", itemIsChecked);
        const newItems = [...items];
        const index = newItems.findIndex(item => item.id === id);
        newItems[index].isChecked = itemIsChecked;
        setItems(newItems);
        if (priorities.some(item => item.taskIdToPriority === newItems[index].id)) {
            const newPriorities = [...priorities];
            const priorityIndex = newPriorities.findIndex(item => item.taskIdToPriority === newItems[index].id);
            newPriorities[priorityIndex].isChecked = itemIsChecked;
            setPriorities(newPriorities);
        }
    }


    return (
        <div className="toDoListStyle" ref={setNodeRef}>
            <h1>To Do List</h1>
            {items && items.length === 0 && <h3>You have no tasks</h3>}
            <SortableContext items={items} strategy={verticalListSortingStrategy}>
                <div>
                    {items.map((item, index) => (
                        <ToDoItem key={item.id} id={item.id} name={item.name} isChecked={item.isChecked} index={index} onNameChange={handleNameChange} onCheck={handleCheckboxChange} onRemove={removeItem} />
                    ))}
                </div>
            </SortableContext>
            <button onClick={addNewItem} data-testid="add-button">New Item</button>
        </div>
    );
};

export default ToDoList;