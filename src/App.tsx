import './App.css';
import ItemProvider from './store/ItemProvider';
import DragAndDrop from './components/DragAndDrop/dragAndDrop';
import React from 'react';
import Header from './components/UI/Header';
import Footer from './components/UI/Footer';


const App: React.FC = () => {
  return (
    <div>
      <div className="App">
        <Header />
        <ItemProvider>
          <DragAndDrop />
        </ItemProvider>
      </div>
      <Footer />
    </div>
  );
}

export default App;
