import {render, screen, fireEvent} from "@testing-library/react";
import '@testing-library/jest-dom'
import ToDoItem from "../../components/ToDo/toDoItem";

describe('ToDoItem Component', () => {
    const id: string = 't1';
    const name: string = 'Task 1';
    const isChecked: boolean = false;
    const index: number = 0;
    const onNameChange = jest.fn();
    const onCheck = jest.fn();
    const onRemove = jest.fn();

    test('Render checkbox and input', () => {
        const { getByRole, getByTestId  } = render(<ToDoItem id={id} name={name} isChecked={isChecked} onNameChange={onNameChange} onCheck={onCheck} onRemove={onRemove} index={index} />);

        // Check if the checkbox is rendered
        const checkbox = getByRole('checkbox');
        expect(checkbox).toBeInTheDocument();

        // Check if the text inputs are rendered
        const todoInput = getByTestId('todoInput');
        expect(todoInput).toBeInTheDocument();
    });

    test('handles checkbox change', () => {
        const { getByRole } = render(<ToDoItem id={id} name={name} isChecked={isChecked} onNameChange={onNameChange} onCheck={onCheck} onRemove={onRemove} index={index} />);
    
        // Simulate a checkbox change event
        const checkbox = getByRole('checkbox') as HTMLInputElement;
        fireEvent.change(checkbox, { target: { checked: true } });
    
        // Check if the state is updated
        expect(checkbox.checked).toBe(true);
      });

      test('handles input change', () => {
        const { getByTestId } = render(<ToDoItem id={id} name={name} isChecked={isChecked} onNameChange={onNameChange} onCheck={onCheck} onRemove={onRemove} index={index} />);
    
        // Simulate an input change event
        const input = getByTestId('todoInput') as HTMLInputElement;
        fireEvent.change(input, { target: { value: 'New Todo' } });
    
        // Check if the state is updated
        expect(input.value).toBe('New Todo');
    });
});