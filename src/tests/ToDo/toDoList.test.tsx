import {render, screen} from "@testing-library/react";
import '@testing-library/jest-dom'
import ToDoList from "../../components/ToDo/toDoList";

test('Render ToDoList component', () => {
    render(<ToDoList />);
    const addButton = screen.getByTestId("add-button");
    expect(addButton).toBeInTheDocument();

    const titleLabel = screen.getByText("To Do List");
    expect(titleLabel).toBeInTheDocument();
})