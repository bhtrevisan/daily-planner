## Daily Planner

## 📜 Description
I'm starting this project for 2 reasons. 
First, I was looking for a simple app to organize some daily activities and I could not find anything I liked.
Second, I want to practice and improve my typescript, so I decided that I was going to do this app using React + Typescript.

The App ideia is to have a To-Do list and a list with my Top 3 tasks I should do.
Then I would like to add a Calendar on half screen to drag and drop this activity.

## 🖼️ Screenshots
I'll add some prints to show how this project looks (when I have something to show).


## 💻 Requirements
You will need:
* npm;
* react;

## 🔨 Installation 
To install this project in your local machine, follow this steps:

Create a folder in your local machine.
Then run

`git clone {this project link}`

Go to project folder.

`npm install`


## ☕ Using Project
To use this project, you need to start this application on your localhost.

Go to project folder, which has te package.json and run:

`npm start`

## 🔧 Fixes and new features
This project is under development and the next steps are:
(I just started, so I will add things here later)
- ...

## 🙂 Author
[@bhtrevisan](https://gitlab.com/bhtrevisan)